package UniqueWords.UniqueWords;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class Parser {
	
	// Считает количество вхождения слова, начиная с элемента k.
	public static int howManyWords(ArrayList<String> words, int k) {
		
		String word = words.get(k);
		
		int n = 1;
		
		for (int i = k + 1; i < words.size(); i++) {
			if (words.get(i).equals(word)) {
				n++;
				words.remove(i);
			}
		}
		
		return n;
	}

	public static String readSite(File file) throws IOException {
		FileReader reader = new FileReader(file);
        
        String site = new String();
        int c;
        while((c=reader.read())!=-1){
        	site += (char)c;
        }
        
        reader.close();
        
        return site;
	}
	
	// Ищет слово в site, разделённое элементом из списка separations, начиная с позиции k.
	public static String findWord(String site, int k, ArrayList<String> separations) {
		String word = new String();
		
		for (int i = k; i < site.length(); i++) {
			String s = new String();
			s += site.charAt(i);
			if (!(separations.contains(s))) { // Если не встретили разделителя, то
				word += s;
			}
			else {
				break;
			}
		}
		
		return word;
	}
	
    public static void main( String[] args ) throws IOException{
    	
        System.out.println("Enter url:");
        String url;
        
        File file = new File("site.txt");
        
        Scanner in = new Scanner(System.in);
        url = in.nextLine();
        in.close();
        
        Page page = new Page(url);
        if (!page.savePage(file)) {
        	System.out.println("Cannot save page!");
        }
        
        String site = readSite(file); // Получили строку с текстом из файла сайта.
        
        final int SIZE_SEPARATIONS = 14;
        ArrayList<String> separations = new ArrayList<String>(SIZE_SEPARATIONS);
        separations.add(" ");
        separations.add(",");
        separations.add(".");
        separations.add("!");
        separations.add("?");
        separations.add("\"\"");
        separations.add(";");
        separations.add(":");
        separations.add("[");
        separations.add("]");
        separations.add("(");
        separations.add(")");
        separations.add("\n");
        separations.add("\r");
        separations.add("\t");
        
        ArrayList<String> words = new ArrayList<String>();
        
        // Нашли все слова.
        int i = 0;
        while (i < site.length()) {
        	words.add(findWord(site, i, separations));
        	i += words.get(words.size() - 1).length() + 1; // уведичиваем на длину последнего слова. С этой позиции будем искать следующее слово.
        }
        
        // Избавились от случайно попавших нулевых элементов.
        for (int j = 0; j < words.size(); j++) {
        	if (words.get(j) == "") {
        		words.remove(j);
        	}
        }
        
        int n = 1;
        
        for (int j = 0; j < words.size(); j++) {
        	n = howManyWords(words, j);
        	System.out.println(words.get(j) + " " + n);
        }
        
    }     
}
