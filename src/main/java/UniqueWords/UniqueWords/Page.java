package UniqueWords.UniqueWords;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Page{
	
	String url;
	Document page;
	
	public Page(String url){
		this.url = url;
		try {
			this.page = Jsoup.parse(new URL(this.url), 3000);
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public boolean savePage(File file) {
		
		String s = this.page.text();
		
		try {
			FileWriter writer = new FileWriter(file, false);
			writer.write(s);
			writer.close();
			
			return true;
		} catch (IOException e) {
			
			return false;
		}
	}
}